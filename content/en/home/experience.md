+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Software Engineer"
  company = "KDAB"
  company_url = "https://www.kdab.com/"
  location = "@home"
  date_start = "2023-10-10"
  description = """
  Consulting on client projects
  """

[[experience]]
  title = "PhD student"
  company = "Inria"
  company_url = "https://maverick.inria.fr/"
  location = "Montbonnot, near Grenoble"
  date_start = "2021-07-15"
  date_end = "2023-10-07"
  description = """
  Pursuing a PhD thesis within a collaboration between the Inria and KDAB on
  automated 3D scene simplification for real-time rendering in a given target
  environnement.
  """

[[experience]]
  title = "Software Engineer"
  company = "KDAB"
  company_url = "https://www.kdab.com/"
  location = "@home"
  date_start = "2020-09-01"
  date_end = "2021-07-14"
  description = """
  Consulting on client projects
  
  Research and develoment around Kuesa, KDAB’s 3D suite
  """

[[experience]]
  title = "6-month internship"
  company = "KDAB"
  company_url = "https://www.kdab.com/"
  location = "Sorgues, near Avignon"
  date_start = "2020-02-03"
  date_end = "2020-07-31"
  description = """
  Improving a graph-based GLSL shader editor
  
  Implementing a Blender extension exporting shader graphs to Qt3D
  """

[[experience]]
  title = "Summer internship"
  company = "RoCamRoll"
  company_url = "https://fr.rocamroll.com/"
  location = "Meylan, near Grenoble"
  date_start = "2019-05-27"
  date_end = "2019-08-09"
  description = """
  Improving a fast Qt-based video editor
  
  Reworking communication between the Erlang backend and Qt frontend
  """

+++
