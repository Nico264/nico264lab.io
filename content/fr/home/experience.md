+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Expérience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Ingénieur Logiciel"
  company = "KDAB France"
  company_url = "https://www.kdab.com/"
  location = "100% télétravail"
  date_start = "2023-10-10"
  description = """
  Conseil et développement sur des projets client
  """

[[experience]]
  title = "Doctorant"
  company = "Inria"
  company_url = "https://maverick.inria.fr/"
  location = "Montbonnot, proche de Grenoble"
  date_start = "2021-07-15"
  date_end = "2023-10-07"
  description = """
  Thèse Cifre en collaboration entre l'Inria et KDAB sur le thème de la
  simplification automatisée de scènes 3D pour le rendu temps réel dans un
  environnement cible donné.
  """

[[experience]]
  title = "Ingénieur Logiciel"
  company = "KDAB France"
  company_url = "https://www.kdab.com/"
  location = "100% télétravail"
  date_start = "2020-09-01"
  date_end = "2021-07-14"
  description = """
  Conseil et développement sur des projets client
  
  Recherche et développement autour de la suite 3D Kuesa
  """

[[experience]]
  title = "Projet de fin d'étude"
  company = "KDAB France"
  company_url = "https://www.kdab.com/"
  location = "Sorgues, proche d'Avignon"
  date_start = "2020-02-03"
  date_end = "2020-07-31"
  description = """
  Reprise et développement d'un éditeur de matériaux basé sur l'édition de graphes.
  
  Définition de shaders pour Qt3D à partir de l'éditeur de graphe de Blender.
  """

[[experience]]
  title = "Stage assistant ingénieur"
  company = "RoCamRoll"
  company_url = "https://fr.rocamroll.com/"
  location = "Meylan, proche de Grenoble"
  date_start = "2019-05-27"
  date_end = "2019-08-09"
  description = """
  Réécriture d'un éditeur vidéo non-linéaire simple et rapide.
  
  Refonte de la communication inter-processus entre un backend Erlang et un frontend Qt/C++.
  """

+++
