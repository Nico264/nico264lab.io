---
# Display name
title: Nicolas Guichard

# Username (this should match the folder name)
authors:
- nicolas.guichard

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ingénieur logiciel

# Organizations/Affiliations
organizations:
- name: KDAB France
  url: "https://www.kdab.com/fr/presentation"

# Short bio (displayed in user profile at end of posts)
# bio:

interests:
- Sécurité
- Logiciel libre
- Systèmes embarqués
- Création et rendu 3D
- Systèmes distribués et réseaux

education:
  courses:
  - course: Diplôme d'ingénieur, mention bien
    institution: Grenoble INP − Ensimag
    year: 2020

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:nicolas@guichard.eu"
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/Nico264
- icon: github
  icon_pack: fab
  link: https://github.com/Nico264
- icon: cv
  icon_pack: ai
  link: /fr/files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
# email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
# - Researchers
# - Visitors
---

