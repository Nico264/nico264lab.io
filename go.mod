module github.com/wowchemy/starter-academic

go 1.16

require github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy/v5 v5.8.1
